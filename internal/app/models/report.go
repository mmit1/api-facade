package models

import (
	"context"
	"gorm.io/gorm"
	"mime/multipart"
)

type Report struct {
	gorm.Model
	ID          int    `gorm:"primaryKey;autoIncrement:true"`
	GoalId      int    `json:"goalId"`
	AuthorId    int    `json:"creatorId"`
	Description string `json:"description"`
	Date        string `json:"date"`

	Images []ReportImg  `json:"photos" gorm:"foreignKey:ReportID"`
	Author        User `gorm:"foreignKey:AuthorId" json:"reportCreator"`
	AuthorAva     string `gorm:"-" json:"imgAvatar"`
}

type ReportUsecase interface {
	SaveReport(ctx context.Context, session string, fileHandlers map[string][]*multipart.FileHeader, Goal *Report) error
	GetReports(c context.Context, session string, goalId int) ([]Report, error)
}

type ReportRepository interface {
	SaveReport(ctx context.Context, Goal *Report, userOwner *User, fileHandlers map[string][]*multipart.FileHeader) error
	GetReports(ctx context.Context, goalId int) ([]Report,error)

}
