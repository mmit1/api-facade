package models

import (
	"context"
	"gorm.io/gorm"
	"mime/multipart"
)

type Goal struct {
	gorm.Model
	ID          int    `gorm:"primaryKey;autoIncrement:true"`
	AuthorId    int    `json:"creatorId"`
	Title       string `json:"title"`
	Description string `json:"description"`
	DateStart   string `json:"dateStart"`
	DateStop    string `json:"dateStop"`
	DateEnd     string `json:"dateEnd"`
	IsClosed    bool   `json:"isClosed"`
	Bet         int    `json:"bet"`
	MoneyPool   int    `json:"moneyPool"`
	Private     string `json:"private"`

	PreviewImages []Img  `json:"photos" gorm:"foreignKey:GoalID"`
	Users         []User `json:"followers" gorm:"many2many:user_goal;primaryKey"`
	Reports []Report `json:"reports" gorm:"foreignKey:GoalId"`
	Author        string `gorm:"-" json:"goalCreator"`
	AuthorAva     string `gorm:"-" json:"authorAvatar"`
	IsMember      bool   `gorm:"-" json:"isMember"`
	//Tags       []Tags `json:"followers" gorm:"many2many:user_goal;primaryKey"`
}

type GoalsUsecase interface {
	CreateGoal(ctx context.Context, session string, fileHandlers map[string][]*multipart.FileHeader, Goal *Goal) error
	GetGoals(ctx context.Context, session string) ([]Goal, error)
	GetUserGoals(c context.Context, session string, goalOwnerID int) ([]Goal, error)
	GetGoal(c context.Context, session string, goalID int) (*Goal, error)

	//Мб стоит в пользователя
	EnterGoal(c context.Context, session string, goalID int) error
	LeaveGoal(c context.Context, session string, goalID int) error
}

type GoalsRepository interface {
	CreateGoal(ctx context.Context, Goal *Goal, userOwner *User, fileHandlers map[string][]*multipart.FileHeader) error
	GetGoals(ctx context.Context) ([]Goal, error)
	GetUserGoals(ctx context.Context, u *User) ([]Goal, error)
	GetGoal(ctx context.Context, id int) (*Goal, error)

	//Мб стоит в пользователя
	EnterGoal(c context.Context, Goal *Goal, user *User) error
	LeaveGoal(c context.Context, userID int, goalID int) error
}
