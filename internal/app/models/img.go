package models

import "gorm.io/gorm"

type Img struct {
	gorm.Model `json:"-"`
	GoalID     int    `json:"-"`
	Url        string `json:"url"`
}
