package models

import "gorm.io/gorm"

type ReportImg struct {
	gorm.Model `json:"-"`
	ReportID     int    `json:"-"`
	Url        string `json:"url"`
}
