package psql

import (
	"context"
	"fmt"
	"gorm.io/gorm"
	"io"
	"mime/multipart"
	"my-motivation/internal/app/models"
	"my-motivation/internal/pkg/utils/hasher"
	"os"
	"time"
)

type GoalRepoPsql struct {
	DB *gorm.DB
}

func NewGoalRepoPsql(db *gorm.DB) *GoalRepoPsql {
	return &GoalRepoPsql{DB: db}
}

//TODO:check rating
func (grp *GoalRepoPsql) CreateGoal(ctx context.Context, newGoal *models.Goal, user *models.User, fileHandlers map[string][]*multipart.FileHeader) error {
	newGoal.AuthorId = user.ID
	newGoal.Users = append(newGoal.Users, *user)
	err := grp.DB.WithContext(ctx).Create(newGoal).Error
	if err != nil {
		return err
	}
	err = grp.DB.WithContext(ctx).Save(&user).Error
	if err != nil {
		return err
	}
	grp.storeImg(newGoal, fileHandlers)
	fmt.Println("Goal added")
	return nil
}

func (grp *GoalRepoPsql) storeImg(newGoal *models.Goal, fileHandlers map[string][]*multipart.FileHeader) error {

	for name, fileHeader := range fileHandlers {
		newImg := models.Img{}
		file, err := fileHeader[0].Open()
		if err != nil {
			return err
		}
		t := time.Now()
		salt := fmt.Sprintf(t.Format(time.RFC3339))
		genFileName := hasher.Hash(name + salt)
		defer file.Close()
		localImg, err := os.OpenFile("./static/goals/"+genFileName+".png", os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			return err
		}
		newImg.Url = "/static/goals/" + genFileName + ".png"
		newImg.GoalID = newGoal.ID
		err = grp.DB.Create(&newImg).Error
		if err != nil {
			return err
		}
		defer localImg.Close()
		_, _ = io.Copy(localImg, file)
	}
	return nil
}

func (grp *GoalRepoPsql) GetGoals(ctx context.Context) ([]models.Goal, error) {
	var Goals []models.Goal
	err := grp.DB.WithContext(ctx).Preload("PreviewImages").Find(&Goals).Error
	if err != nil {
		return nil, err
	}
	for i, _ := range Goals {
		u := models.User{}
		err = grp.DB.WithContext(ctx).First(&u, "id = ?", Goals[i].AuthorId).Error
		if err != nil {
			return nil, err
		}
		Goals[i].Author = u.FirstName + " " + u.LastName
		Goals[i].AuthorAva = u.Avatar
	}
	return Goals, nil
}

func (grp *GoalRepoPsql) GetUserGoals(ctx context.Context, u *models.User) ([]models.Goal, error) {
	var Goals []models.Goal
	err := grp.DB.WithContext(ctx).Preload("PreviewImages").Where("author_id = ?", u.ID).Find(&Goals).Error
	if err != nil {
		return nil, err
	}
	return Goals, nil
}

func (grp *GoalRepoPsql) GetGoal(ctx context.Context, goalId int) (*models.Goal, error) {
	goal := &models.Goal{}
	goal.ID = goalId
	err := grp.DB.WithContext(ctx).Preload("PreviewImages").Preload("Users").First(goal).Error
	if err != nil {
		return nil, err
	}

	return goal, nil
}

//TODO:check rating
func (grp *GoalRepoPsql) EnterGoal(ctx context.Context, goal *models.Goal, user *models.User) error {
	err := grp.DB.WithContext(ctx).Model(goal).Association("Users").Append(user)
	if err != nil {
		return err
	}
	err = grp.DB.WithContext(ctx).Save(user).Error
	if err != nil {
		return err
	}
	err = grp.DB.WithContext(ctx).Save(goal).Error
	if err != nil {
		return err
	}
	return nil
}

func (grp *GoalRepoPsql) LeaveGoal(ctx context.Context, userID int, goalId int) error {
	goal := &models.Goal{}
	goal.ID = goalId
	err := grp.DB.WithContext(ctx).Model(&goal).Association("Users").Delete(&models.User{ID: userID})
	if err != nil {
		return err
	}
	return nil
}
