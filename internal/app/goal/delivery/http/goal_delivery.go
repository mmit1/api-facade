package http

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"my-motivation/internal/app/models"
	"my-motivation/internal/pkg/utils/logger"
	"net/http"
	"strconv"
)

type GoalHandler struct {
	GoalUsecase models.GoalsUsecase
	logger logger.LoggerModel
}

func NewGoalHandler(r *mux.Router, pu models.GoalsUsecase, l logger.LoggerModel) {
	handler := &GoalHandler{
		GoalUsecase: pu,
		logger: l,
	}
	r.HandleFunc("/goals", handler.allGoals).Methods("GET", "OPTIONS")
	r.HandleFunc("/user/goals/{userID}", handler.userGoals).Methods("GET", "OPTIONS")
	r.HandleFunc("/goal/{goalID}", handler.getGoal).Methods("GET", "OPTIONS")

	r.HandleFunc("/goal/create", handler.createGoal).Methods("POST", "OPTIONS")
	r.HandleFunc("/goal/{goalID}/enter", handler.enterGoal).Methods("POST", "OPTIONS")
	r.HandleFunc("/goal/{goalID}/leave", handler.leaveGoal).Methods("POST", "OPTIONS")
}


//TODO:рейтинг
func (ph *GoalHandler) enterGoal(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	goalId, err := strconv.Atoi(mux.Vars(r)["goalID"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	err = ph.GoalUsecase.EnterGoal(r.Context(), session.Value, goalId)
	if err != nil {
		ph.logger.Error(err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
}

//TODO:рейтинг
func (ph *GoalHandler) leaveGoal(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	fmt.Println("here")

	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	goalId, err := strconv.Atoi(mux.Vars(r)["goalID"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	err = ph.GoalUsecase.LeaveGoal(r.Context(), session.Value, goalId)
	if err != nil {
		ph.logger.Error(err.Error())
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (ph *GoalHandler) userGoals(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	userId, err := strconv.Atoi(mux.Vars(r)["userID"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	Goals, err := ph.GoalUsecase.GetUserGoals(r.Context(), session.Value, userId)
	if err != nil {
		ph.logger.Error(err.Error())
		return
	}
	jsonValue, _ := json.Marshal(Goals)
	ph.logger.Debug("Goals sends")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonValue))
}

func (ph *GoalHandler) allGoals(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	Goals, err := ph.GoalUsecase.GetGoals(r.Context(), session.Value)
	if err != nil {
		ph.logger.Error(err.Error())
		return
	}
	jsonValue, _ := json.Marshal(Goals)
	ph.logger.Debug("Goals sends")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonValue))
}

func (ph *GoalHandler) getGoal(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	goalID, err := strconv.Atoi(mux.Vars(r)["goalID"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	goal, err := ph.GoalUsecase.GetGoal(r.Context(), session.Value, goalID)
	if err != nil {
		ph.logger.Error(err.Error())
		return
	}
	jsonValue, _ := json.Marshal(goal)
	ph.logger.Debug("Goals sends")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonValue))
}


func (ph *GoalHandler) createGoal(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	mr, err := r.MultipartReader()
	form, err := mr.ReadForm(100000)
	if err != nil {
		ph.logger.Error("Files error")
		w.WriteHeader(http.StatusForbidden)
		return
	}

	newGoal := models.Goal{}
	newGoal.Title = form.Value["title"][0]
	newGoal.Description = form.Value["description"][0]
	newGoal.DateStart = form.Value["dateStart"][0]
	newGoal.DateEnd = form.Value["dateEnd"][0]
	newGoal.Bet,_ = strconv.Atoi(form.Value["bet"][0])
	newGoal.Private = form.Value["privateStatus"][0]

	fmt.Printf("%+v\n", newGoal)


	err = ph.GoalUsecase.CreateGoal(r.Context(), session.Value, form.File, &newGoal)

	if err != nil {
		ph.logger.Error(err.Error())
		w.WriteHeader(http.StatusForbidden)
		return
	}

	w.WriteHeader(http.StatusOK)
}