package usecase

import (
	"context"
	"errors"
	"mime/multipart"
	"my-motivation/internal/app/models"
	_sessionManager "my-motivation/internal/app/session/psql"
	user_service_client "my-motivation/internal/pkg/client/user-service-client"
	"time"
)

type GoalUsecase struct {
	UserRepo       models.UserRepository
	GoalRepo       models.GoalsRepository
	contextTimeout time.Duration
	sessionManager *_sessionManager.SessionsManagerPsql
	userService     *user_service_client.Client
}

func NewGoalUsecase(ur models.UserRepository, pr models.GoalsRepository, timeout time.Duration, sm *_sessionManager.SessionsManagerPsql, ac *user_service_client.Client) models.GoalsUsecase {
	return &GoalUsecase{
		UserRepo:       ur,
		GoalRepo:       pr,
		contextTimeout: timeout,
		sessionManager: sm,
		userService: ac,
	}
}

func (gu *GoalUsecase) CreateGoal(c context.Context, session string, fileHandlers map[string][]*multipart.FileHeader, Goal *models.Goal) error {
	user, err := gu.userService.GetUserBySession(c, session)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(c, gu.contextTimeout)
	defer cancel()

	userOwner, err := gu.UserRepo.GetUserById(ctx, user.ID)
	if err != nil || userOwner == nil {
		return err
	}

	if userOwner.Rating < Goal.Bet {
		return errors.New("Деняг нет")
	}

	Goal.MoneyPool += Goal.Bet
	userOwner.Rating -= Goal.Bet

	err = gu.GoalRepo.CreateGoal(ctx, Goal, userOwner, fileHandlers)
	if err != nil {
		return err
	}

	return nil
}

//TODO:оптимизировать
func (gu *GoalUsecase) GetGoals(c context.Context, session string) ([]models.Goal, error) {
	user, err := gu.userService.GetUserBySession(c, session)
	if err != nil {
		return nil,err
	}
	ctx, cancel := context.WithTimeout(c, gu.contextTimeout)
	defer cancel()

	_, err = gu.UserRepo.GetUserById(ctx, user.ID)
	if err != nil {
		return nil, err
	}

	Goals, err := gu.GoalRepo.GetGoals(ctx)
	if err != nil {
		return nil, err
	}

	UserGoals, err := gu.UserRepo.GetGoals(ctx, user.ID)
	if err != nil {
		return nil, err
	}

	for _, userGoal := range UserGoals {
		for i, goal := range Goals {
			if goal.ID == userGoal.ID {
				Goals[i].IsMember = true
			}
		}
	}

	return Goals, nil
}

func (gu *GoalUsecase) GetUserGoals(c context.Context, session string, goalOwnerID int) ([]models.Goal, error) {
	_, err := gu.userService.GetUserBySession(c, session)
	if err != nil {
		return nil,err
	}

	ctx, cancel := context.WithTimeout(c, gu.contextTimeout)
	defer cancel()

	goalOwner, err := gu.UserRepo.GetUserById(ctx, goalOwnerID)
	if err != nil {
		return nil, err
	}

	Goals, err := gu.GoalRepo.GetUserGoals(ctx, goalOwner)
	if err != nil {
		return nil, err
	}

	return Goals, nil
}

func (gu *GoalUsecase) GetGoal(c context.Context, session string, goalID int) (*models.Goal, error) {
	user, err := gu.userService.GetUserBySession(c, session)
	if err != nil {
		return nil,err
	}
	ctx, cancel := context.WithTimeout(c, gu.contextTimeout)
	defer cancel()

	_, err = gu.UserRepo.GetUserById(ctx, user.ID)
	if err != nil {
		return nil, err
	}

	goal, err := gu.GoalRepo.GetGoal(ctx, goalID)
	if err != nil {
		return nil, err
	}

	author, err := gu.UserRepo.GetUserById(ctx, goal.AuthorId)
	if err != nil {
		return nil, err
	}

	for _, usr := range goal.Users {
		if usr.ID == user.ID {
			goal.IsMember = true
		}
	}

	goal.Author = author.FirstName + " " + author.LastName
	goal.AuthorAva = author.Avatar
	return goal, nil
}

func (gu *GoalUsecase) EnterGoal(ctx context.Context, session string, goalID int) error {
	user, err := gu.userService.GetUserBySession(ctx, session)
	if err != nil {
		return err
	}

	u, err := gu.UserRepo.GetUserById(ctx, user.ID)
	if err != nil {
		return err
	}

	g, err := gu.GoalRepo.GetGoal(ctx, goalID)
	if err != nil {
		return err
	}

	if u.Rating < g.Bet {
		return errors.New("Деняг нет")
	}

	g.MoneyPool += g.Bet
	u.Rating -= g.Bet

	err = gu.GoalRepo.EnterGoal(ctx, g, u)
	if err != nil {
		return err
	}
	return nil
}

func (gu *GoalUsecase) LeaveGoal(ctx context.Context, session string, goalID int) error {
	user, err := gu.userService.GetUserBySession(ctx, session)

	if err != nil {
		return err
	}
	err = gu.GoalRepo.LeaveGoal(ctx, user.ID, goalID)
	if err != nil {
		return err
	}
	return nil
}
