package usecase

import (
	"context"
	"mime/multipart"
	"my-motivation/internal/app/models"
	_sessionManager "my-motivation/internal/app/session/psql"
	user_service_client "my-motivation/internal/pkg/client/user-service-client"
	"time"
)

type ReportUsecase struct {
	UserRepo       models.UserRepository
	GoalRepo       models.GoalsRepository
	ReportRepo     models.ReportRepository
	contextTimeout time.Duration
	sessionManager *_sessionManager.SessionsManagerPsql
	userService    *user_service_client.Client
}

func NewReportUsecase(ur models.UserRepository, gr models.GoalsRepository, rr models.ReportRepository, timeout time.Duration, sm *_sessionManager.SessionsManagerPsql, ac *user_service_client.Client) models.ReportUsecase {
	return &ReportUsecase{
		UserRepo:       ur,
		GoalRepo:       gr,
		ReportRepo: rr,
		contextTimeout: timeout,
		sessionManager: sm,
		userService:    ac,
	}
}

func (gu *ReportUsecase) GetReports(c context.Context, session string, goalId int) ([]models.Report, error) {
	_, err := gu.userService.GetUserBySession(c, session)
	if err != nil {
		return nil,err
	}

	ctx, cancel := context.WithTimeout(c, gu.contextTimeout)
	defer cancel()

	reports, err := gu.ReportRepo.GetReports(ctx, goalId)
	if err != nil {
		return nil, err
	}

	return reports, nil
}

func (ru *ReportUsecase) SaveReport(c context.Context, session string, fileHandlers map[string][]*multipart.FileHeader, report *models.Report) error {
	user, err := ru.userService.GetUserBySession(c, session)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(c, ru.contextTimeout)
	defer cancel()

	goal, err := ru.GoalRepo.GetGoal(ctx, report.GoalId)
	if err != nil || goal == nil {
		return err
	}

	err = ru.ReportRepo.SaveReport(ctx, report, user, fileHandlers)
	if err != nil {
		return err
	}

	return nil
}
