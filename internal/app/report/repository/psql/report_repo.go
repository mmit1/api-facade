package psql

import (
	"context"
	"fmt"
	"gorm.io/gorm"
	"io"
	"mime/multipart"
	"my-motivation/internal/app/models"
	"my-motivation/internal/pkg/utils/hasher"
	"os"
	"time"
)

type ReportRepoPsql struct {
	DB *gorm.DB
}

func NewReportRepoPsql(db *gorm.DB) *ReportRepoPsql {
	return &ReportRepoPsql{DB: db}
}

func (rrp *ReportRepoPsql) SaveReport(ctx context.Context, newReport *models.Report, userOwner *models.User, fileHandlers map[string][]*multipart.FileHeader) error {
	newReport.AuthorId = userOwner.ID
	err := rrp.DB.WithContext(ctx).Create(newReport).Error
	if err != nil {
		return err
	}
	rrp.storeImg(newReport, fileHandlers)
	return nil
}

func (rrp *ReportRepoPsql) GetReports(ctx context.Context, goalId int) ([]models.Report,error) {
	reports := []models.Report{}
	err := rrp.DB.WithContext(ctx).Where("goal_id = ?", goalId).Preload("Images").Preload("Author").Find(&reports).Error
	if err != nil {
		return nil, err
	}
	return reports, nil
}

func (rrp *ReportRepoPsql) storeImg(newReport *models.Report, fileHandlers map[string][]*multipart.FileHeader) error {
	for name, fileHeader := range fileHandlers {
		newImg := models.ReportImg{}
		file, err := fileHeader[0].Open()
		if err != nil {
			return err
		}
		t := time.Now()
		salt := fmt.Sprintf(t.Format(time.RFC3339))
		genFileName := hasher.Hash(name + salt)
		defer file.Close()
		localImg, err := os.OpenFile("./static/report/"+genFileName + ".png", os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			return err
		}
		newImg.Url = "/static/report/" + genFileName + ".png"
		newImg.ReportID = newReport.ID
		err = rrp.DB.Create(&newImg).Error
		if err != nil {
			return err
		}
		defer localImg.Close()
		_, _ = io.Copy(localImg, file)
	}
	return nil
}
//
//func (grp *GoalRepoPsql) GetGoals(ctx context.Context) ([]models.Goal, error) {
//	var Goals []models.Goal
//	err := grp.DB.WithContext(ctx).Preload("PreviewImages").Find(&Goals).Error
//	if err != nil {
//		return nil, err
//	}
//	for i, _ := range Goals {
//		u := models.User{}
//		err = grp.DB.WithContext(ctx).First(&u, "id = ?", Goals[i].AuthorId).Error
//		if err != nil {
//			return nil, err
//		}
//		Goals[i].Author = u.FirstName + " " + u.LastName
//		Goals[i].AuthorAva = u.Avatar
//	}
//	return Goals, nil
//}
//
//func (grp *GoalRepoPsql) GetUserGoals(ctx context.Context, u *models.User) ([]models.Goal, error) {
//	var Goals []models.Goal
//	err := grp.DB.WithContext(ctx).Preload("PreviewImages").Where("author_id = ?", u.ID).Find(&Goals).Error
//	if err != nil {
//		return nil, err
//	}
//	return Goals, nil
//}
//
//func (grp *GoalRepoPsql) GetGoal(ctx context.Context, goalId int) (*models.Goal, error) {
//	goal := &models.Goal{}
//	goal.ID = goalId
//	err := grp.DB.WithContext(ctx).Preload("PreviewImages").Preload("Users").First(goal).Error
//	if err != nil {
//		return nil, err
//	}
//	return goal, nil
//}
//
//func (grp *GoalRepoPsql) EnterGoal(ctx context.Context, userID int, goalId int) error {
//	goal := &models.Goal{}
//	goal.ID = goalId
//	grp.DB.WithContext(ctx).Model(&goal).Association("Users").Append(&models.User{ID: userID})
//	err := grp.DB.WithContext(ctx).Preload("PreviewImages").Preload("Users").First(goal).Error
//	if err != nil {
//		return err
//	}
//	return nil
//}
