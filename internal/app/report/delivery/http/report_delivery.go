package http

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"my-motivation/internal/app/models"
	"my-motivation/internal/pkg/utils/logger"
	"net/http"
	"strconv"
)

type ReportHandler struct {
	ReportUsecase models.ReportUsecase
	logger logger.LoggerModel
}

func NewReportHandler(r *mux.Router, ru models.ReportUsecase, l logger.LoggerModel) {
	handler := &ReportHandler{
		ReportUsecase: ru,
		logger: l,
	}

	r.HandleFunc("/report/create", handler.saveReport).Methods("POST", "OPTIONS")
	r.HandleFunc("/goal/reports/{goalId}", handler.getReports).Methods("GET", "OPTIONS")
}

func (ph *ReportHandler) getReports(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	session, err := r.Cookie("session_id")
	if err != nil {
		ph.logger.Error("no authorization")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	goalId, err := strconv.Atoi(mux.Vars(r)["goalId"])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	Goals, err := ph.ReportUsecase.GetReports(r.Context(), session.Value, goalId)
	if err != nil {
		ph.logger.Error(err.Error())
		return
	}
	jsonValue, _ := json.Marshal(Goals)
	ph.logger.Debug("Reports sends")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(jsonValue))
}

func (rh *ReportHandler) saveReport(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	session, err := r.Cookie("session_id")
	if err != nil {
		rh.logger.Error("no authorization")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	mr, err := r.MultipartReader()
	form, err := mr.ReadForm(100000)
	if err != nil {
		rh.logger.Error("Files error")
		w.WriteHeader(http.StatusForbidden)
		return
	}

	newReport := models.Report{}
	newReport.Description = form.Value["description"][0]
	newReport.Date = form.Value["date"][0]
	newReport.GoalId, _ = strconv.Atoi(form.Value["goalId"][0])
	fmt.Printf("%+v\n", newReport)

	err = rh.ReportUsecase.SaveReport(r.Context(), session.Value, form.File, &newReport)

	if err != nil {
		rh.logger.Error(err.Error())
		w.WriteHeader(http.StatusForbidden)
		return
	}
	rh.logger.Info("report add success")
	w.WriteHeader(http.StatusOK)
}